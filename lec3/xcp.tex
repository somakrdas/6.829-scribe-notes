\section{eXplicit Control Protocol (XCP)}
XCP is a new take on solving the problem of Internet congestion that makes TCP inefficient and prone to instability, especially for links with high bandwidth-delay products. XCP is modeled using \emph{Control Theory} and is shown to perform better than TCP in both conventional and high bandwidth-delay environments. The main achievements of XCP are fair bandwidth allocation, high throughput utilization, small persistent queue size, and near-zero packet drops with both steady and highly varying traffic.

There are several main ideas to take away from XCP, listed below.
\subsection{Decoupling controllers for efficiency and fairness in a scalable manner}
  No Active Queue Management (AQM) controller with constant parameters can be fast enough to operate with an arbitrary number of TCP flows, which is often the case in the real world. This necessitates the decoupling of efficiency control from fairness control.  
  
  XCP performs this decoupling by performing efficiency control for the aggregrate flow and having a different controller for regulating the fair division of throughput between the different flows. This ensures that robustness to congestion does not vary with the number of flows through the link. This idea is not a new one - indeed, a similar kind of decoupling was used in ATM networks although using states and queueing mechanisms for each flow through a router.
  
  XCP, however, does not use any such individual states, and is thus scalable. Each router adds information in packet headers to keep track of the dynamic packet states and to pass information to the end-points. This is certainly more efficient than having a state for each flow throught the router. In this manner, most of the computation is shifted to the end points and the process of designing the routers to operate at high speeds becomes simpler.

%  \includegraphics{} TODO' : picture of XCP packet header

 \subsection{Non-binary notification}
 TCP Reno handles congestion by either marking or dropping packets. While this informs the sender when congestion occurs, it is the left to the sender to choose an appropriate update to its congestion window.
 
 XCP introduces the concept of providing more than just two kinds of information to the sender i.e whether congestion exists or not. The router marks packet headers with explicit information of how the sender must respond to take advantage of spare bandwidth or avoid congestion. On the whole, in XCP, the sender receives feedback on dropped packets, whether congestion exists, and the amount of throughput left for it to utilize. The calculations performed by the router to determine the numbers to put in each packet header are not complicated and hence, ensure its fast operation, which is vital for scalability.
 
  The spare capacity (throughput) available to the link serviced by the router is given by :
  \begin{equation}
   \phi = \alpha.d.spare - \beta.Q %TODO
  \end{equation}
  where $\alpha$ and $\beta$ are parameters that need to be chosen and $Q$ is the \emph{persistent} queue size.

  In the case that $\phi$ is positive, the throughput increase must occur equally for all the flows. Hence, the congestion window for flow $i$, $cwnd_i$ must increase $\propto rtt_i$. In terms of increase per packet, this gives us, for flow $i$ :
  \begin{equation}
   increase/pkt  \propto \frac{rtt_i^2}{cwnd_i}.s_i
  \end{equation}
  
  The number of packets in flow $i$ in a time interval $d$ is given by:
      \begin{equation}
       p_i^d = \frac{d.cwnd_i}{rtt.s_i}
      \end{equation}
  where $rtt$ is the round trip time, $cwnd_i$ is the size of the congestion window and $s_i$ is the packet size for flow $i$.
  

  \subsection{Complete Protocol Redesign}
  XCP is a completely redesigned protocol from explicit \emph{first principles}. Backward compatibility with TCP is not considered - this marks a new line of thinking that indicates that a complete overhaul of TCP might be required as the size of the internet grows, even though the task is a formidable one.
  
  \subsection{Theoretical Backing}
  The paper uses control theory to provide theoretical support for the stability of XCP. This analysis is used in choosing values for the free parameters $\alpha$ and $\beta$. Note that choosing parameter values is often a difficult task since it is usually done experimentally and varies widely according to the type of network under consideration. A theoretical method is thus, highly useful.
  
  \begin{theorem}
   Suppose the round trip delay is $d$. If the parameters $\alpha$ and $\beta$ satisfy
   \begin{equation}
    0 < \alpha < \frac{\pi}{4\sqrt{2}} \text{ and }  \beta = \alpha^2\sqrt{2}
   \end{equation}
   then the system is stable independently of delay, capacity, and number of sources (flows).

  \end{theorem}

